package main

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/robfig/cron"
)

var (
	botCfg Config
)

type Update struct {
	ChannelID string `json:"chanid"`
	MessageID string `json:"msgid"`
}

type Config struct {
	Token   string   `json:"token"`
	Updates []Update `json:"updates"`
}

func init() {
	fmt.Println("Loading config and parsing config...")
	conf, e := os.Open("config.json")
	if e != nil {
		fmt.Printf("[error] error opening config.json: %s\n", e.Error())
		os.Exit(1)
	}
	cfgParser := json.NewDecoder(conf)
	if e = cfgParser.Decode(&botCfg); e != nil {
		fmt.Printf("[error] error parsing config.json: %s\n", e.Error())
		os.Exit(1)
	}
}

func main() {
	// Time calculation stuff
	d, h, m := calcUntil()

	s := fmt.Sprintf("%d days, %.0f hours, %.0f minutes remaining", d, h, m)
	fmt.Println(s)

	// Discord stuff here
	dg, e := discordgo.New(fmt.Sprintf("Bot %s", botCfg.Token))
	if e != nil {
		fmt.Printf("[error] error getting new session: %s\n", e.Error())
		return
	}

	// Add handlers
	dg.AddHandler(newMessage)
	dg.AddHandler(discordReady)

	e = dg.Open()
	if e != nil {
		fmt.Printf("[error] error opening client: %s\n", e.Error())
		return
	}

	fmt.Printf("Logged in as %s, hit CTRL-C to stop\n", dg.State.User.Username)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

// new message handler
func newMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Don't respond to bots
	if m.Author.Bot {
		return
	}

	switch m.Content {
	case ".until":
		days, hours, mins := calcUntil()
		s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("There are %d days, %.0f hours and %.0f minutes left!", days, hours, mins))
		return
	default:
		return
	}
}

// ready event handler
func discordReady(s *discordgo.Session, r *discordgo.Ready) {
	cj := cron.New()
	fmt.Println("ready event fired")
	doUpdate(s)
	updateStatus(s)
	cj.AddFunc("@every 5m", func() {
		doUpdate(s)
	})
	cj.AddFunc("@every 1m", func() {
		updateStatus(s)
	})
	cj.Run()
}

func updateStatus(s *discordgo.Session) {
	d, h, m := calcUntil()
	s.UpdateStatus(0, fmt.Sprintf("%d days, %.0f hours and %.0f minutes remaining.", d, h, m))
	var str string
	if h == 0 && m == 0 {
		switch d {
		case 2:
			// Third day
			str = "Dawn of the Third Day\n ***- 72 hours remain -***"
		case 1:
			// Second day
			str = "Dawn of the Second Day\n ***- 48 hours remain -***"
		case 0:
			// Final day
			str = "Dawn of the Final Day\n ***= 24 hours remain -***"
		default:
			// None of the above
			return
		}
		for i := 0; i < len(botCfg.Updates); i++ {
			u := botCfg.Updates[i]
			_, e := s.ChannelMessageSend(u.ChannelID, str)
			if e != nil {
				fmt.Printf("[error] failed to send '%s' to channel ID '%s': %s\n", str, u.ChannelID, e.Error())
			}
		}
	}
}

func doUpdate(s *discordgo.Session) {
	d, h, m := calcUntil()
	for i := 0; i < len(botCfg.Updates); i++ {
		u := botCfg.Updates[i]
		_, e := s.ChannelMessageEdit(u.ChannelID, u.MessageID, fmt.Sprintf("%d days, %.0f hours and %.0f minutes remaining.", d, h, m))
		if e != nil {
			fmt.Printf("[error] failed to update message with ID '%s' in channel '%s': %s\n", u.MessageID, u.ChannelID, e.Error())
		}
	}
}

func calcUntil() (days int, hours, minutes float64) {
	startDay, _ := time.Parse(time.RFC3339, "2018-09-04T04:00:00.000Z")
	until := time.Until(startDay)

	days = int(until.Hours() / 24)
	hours = math.Mod(until.Hours(), 24)
	minutes = math.Mod(hours*60, 60)

	return days, hours, minutes
}
